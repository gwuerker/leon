import util

from flask import Flask, render_template

leon = Flask('leon', static_folder='static', template_folder='templates')

@leon.route('/')
def home():
	return render_template('home.tmpl')

@leon.route('/users')
def users():
	jobs = util.get_jobs()
	users = util.get_users(jobs)
	
	params = {
		'users' : users,
		'util'  : util
	}
	return render_template('users.tmpl', **params)

@leon.route('/user/<id>')
def user(id):
	jobs = util.get_jobs()
	user_jobs = util.filter_jobs(id, jobs)
	user_jobs.reverse()
	
	params = {
		'user'	    : id,
		'user_jobs' : user_jobs,
		'util' 	    : util
	}
	return render_template('user.tmpl', **params)

if __name__ == '__main__':
	leon.debug = True
	leon.run(host='0.0.0.0', port=52020)
