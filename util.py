import mysql.connector
import pwd
import datetime


# organizers
def filter_jobs(user, jobs):
	user_jobs = []
	for job in jobs:
		if user == job['user']:
			user_jobs.append(job)

	return user_jobs

def summarize_jobs(jobs):
	job_summary = {}	
	job_summary['job_count'] = len(jobs)
	job_summary['cpu_time'] = 0

	for job in jobs:
		job_summary['cpu_time'] += job['cpu_time']
	
	return job_summary


# database functions
def create_con():
	return mysql.connector.connect(user='slurm', password='slurmP@$$!', host='127.0.0.1', database='slurm_acct_db')

def get_jobs():
	con = create_con()
	cur = con.cursor(buffered=True)
	cur.execute('select * from bgsc_job_table')
	
	# create empty list for jobs
	jobs = []

	for result in cur:
		job = {}
		# convert user_id to user_name
		job['user'] = pwd.getpwuid(result[20]).pw_name		
		job['job_name'] = result[11]
		job['job_id'] = result[16]
		job['start'] = result[33]
		job['end'] = result[34]
		job['runtime'] = job['end'] - job['start']
		job['nodelist'] = result[24]
		job['cpus_req'] = result[7]
		job['cpu_time'] = job['runtime'] * job['cpus_req']
		jobs.append(job)		

	con.close()
	return jobs

def get_users(jobs):
	# get user names
	user_names = []
	for job in jobs:
		if job['user'] not in user_names:
			user_names.append(job['user'])
	
	# create user object
	users = []
	for user_name in user_names:
		user = {'user' : user_name}
		jobs_summary = summarize_jobs(filter_jobs(user_name, jobs))
		user['job_count'] = jobs_summary['job_count']
		user['cpu_time'] = jobs_summary['cpu_time']
		users.append(user)
	
	return users

 
# formatters
def format_date(time):
	return datetime.datetime.fromtimestamp(time).strftime('%m/%d/%y %H:%M:%S') 

def format_time(time):
	m, s = divmod(time, 60)
	h, m = divmod(m, 60)
	return '%d:%02d:%02d' % (h, m, s)

